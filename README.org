#+TITLE: LibWeb - library, that implements Web standarts

** What standarts are implementing?
- HTML

** What can this library do now?

LibWeb can tokenize and parse HTML to DOM tree, that you can use in your applications.

** How to build?

- `cd src`
- `make`

If you want to also build utilities:

- `make utils`

After this `libweb.a` will be in `builds` directory and utils in `build/utils`
