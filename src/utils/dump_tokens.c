#include <unistd.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/mman.h>
#include <LibWeb/HTML/tokenizer.h>
#include <LibWeb/HTML/token.h>
#include <LibWeb/HTML/parser.h>

int main(int argc, char *argv[]) {
  if (argc < 2) {
    fprintf(stderr, "Not enough arguments\n");
    return 1;
  }
	int fd = open(argv[1], O_RDONLY); 
  if (fd < 0) {
    fprintf(stderr, "Failed to open file: %s\n", argv[1]);
    return 1;
  }
  size_t size = lseek(fd, 0, SEEK_END);
  char *data = mmap(0, size, PROT_READ, MAP_PRIVATE, fd, 0);
  while(dump_token(next_token(data)) != -1);
}
