#ifndef LIBWEB_STACK_H
#define LIBWEB_STACK_H

#include <LibWeb/DOM/node.h>

typedef Node** stack_t;

Node* last_node_on_stack();
Node* pop();
void push(Node* element);

#endif
