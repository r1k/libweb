#ifndef LIBWEB_DOM_DOCUMENT_H
#define LIBWEB_DOM_DOCUMENT_H

#include <LibWeb/DOM/types.h>
#include <LibWeb/DOM/document_type.h>
#include <LibWeb/DOM/element.h>
#include <LibWeb/DOM/text.h>

//interface Document : Node {
//  constructor();
typedef struct Document {
  //[SameObject] readonly attribute DOMImplementation implementation;

  /*
  readonly attribute USVString URL;
  readonly attribute USVString documentURI;*/
  DOMString compat_mode;
  DOMString character_set;
  DOMString charset; // legacy alias of .characterSet
  DOMString input_encoding; // legacy alias of .characterSet
  DOMString content_type;

  DocumentType doctype;
  Element document_element;

  // FIXME:
  Text text;
} Document;
/*
  HTMLCollection getElementsByTagName(DOMString qualifiedName);
  HTMLCollection getElementsByTagNameNS(DOMString? namespace, DOMString localName);
  HTMLCollection getElementsByClassName(DOMString classNames);
*/

// TODO: [CEReactions, NewObject] Element createElement(DOMString localName, optional (DOMString or ElementCreationOptions) options = {});

Element create_element(Document document, DOMString local_name); // FIXME: options

/*  [CEReactions, NewObject] Element createElementNS(DOMString? namespace, DOMString qualifiedName, optional (DOMString or ElementCreationOptions) options = {});
  [NewObject] DocumentFragment createDocumentFragment();
  [NewObject] Text createTextNode(DOMString data);
  [NewObject] CDATASection createCDATASection(DOMString data);
  [NewObject] Comment createComment(DOMString data);
  [NewObject] ProcessingInstruction createProcessingInstruction(DOMString target, DOMString data);

  [CEReactions, NewObject] Node importNode(Node node, optional boolean deep = false);
  [CEReactions] Node adoptNode(Node node);

  [NewObject] Attr createAttribute(DOMString localName);
  [NewObject] Attr createAttributeNS(DOMString? namespace, DOMString qualifiedName);

  [NewObject] Event createEvent(DOMString interface); // legacy

  [NewObject] Range createRange();

  // NodeFilter.SHOW_ALL = 0xFFFFFFFF
  [NewObject] NodeIterator createNodeIterator(Node root, optional unsigned long whatToShow = 0xFFFFFFFF, optional NodeFilter? filter = null);
  [NewObject] TreeWalker createTreeWalker(Node root, optional unsigned long whatToShow = 0xFFFFFFFF, optional NodeFilter? filter = null);
};
*/

#endif
