#ifndef LIBWEB_DOM_TEXT_H
#define LIBWEB_DOM_TEXT_H

#include <LibWeb/DOM/character_data.h>

typedef struct Text {
  CharacterData data;
  DOMString whole_text;
} Text;

Text text_constructor(Text text, DOMString data);

// Wrappers for CharacterNode functions
Text text_append(Text text, DOMString data);

#endif
