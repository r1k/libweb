#ifndef LIBWEB_DOM_DOCUMENT_TYPE_H
#define LIBWEB_DOM_DOCUMENT_TYPE_H

#include <LibWeb/DOM/types.h>

typedef struct DocumentType {
  DOMString name;
  DOMString public_id;
  DOMString system_id;
} DocumentType;

#endif
