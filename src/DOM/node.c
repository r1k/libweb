#include <LibWeb/DOM/node.h>
#include <LibWeb/HTML/stack.h>
#include <stddef.h>
#include <stdlib.h>

bool is_element_node(Node *node) {
  return node->node_type == ELEMENT_NODE;
}

bool is_attribute_node(Node *node) {
  return node->node_type == ATTRIBUTE_NODE;
}

bool is_text_node(Node *node) {
  return node->node_type == TEXT_NODE;
}

bool is_cdata_section_node(Node *node) {
  return node->node_type == CDATA_SECTION_NODE;
}

bool is_processing_instruction_node(Node *node) {
  return node->node_type == PROCESSING_INSTRUCTION_NODE;
}

bool is_comment_node(Node *node) {
  return node->node_type == COMMENT_NODE;
}

bool is_document_node(Node *node) {
  return node->node_type == DOCUMENT_NODE;
}

bool is_document_type_node(Node *node) {
  return node->node_type == DOCUMENT_TYPE_NODE;
}

bool is_document_fragment_node(Node *node) {
  return node->node_type == DOCUMENT_FRAGMENT_NODE;
}

Node* init_document_node() {
  Node *document_node = malloc(sizeof(Node));
  document_node->node_type = DOCUMENT_NODE;
  // FIXME: node name
  document_node->parent_node = NULL;
  document_node->first_child = document_node->last_child = document_node->previous_sibling = document_node->next_sibling = NULL;
  return document_node;
}

Element create_element_for_token(Node* intended_parent, token_t token) {
  // TODO: reimplement - https://html.spec.whatwg.org/multipage/parsing.html#create-an-element-for-the-token
  
  Document document = intended_parent->owner_document;
  document.content_type = "html"; // FIXME
  DOMString local_name = tag_name(token); 

  Element element = create_element(document, local_name);

  return element;
}

Node* make_node(node_type_t node_type, token_t token) {
  Node *tmp = malloc(sizeof(Node));
  tmp->node_type = node_type;
  tmp->parent_node = NULL;
  tmp->first_child = tmp->last_child = tmp->previous_sibling = tmp->next_sibling = NULL;

  Document tmp_document;

  switch (node_type) {
  case ELEMENT_NODE:
    tmp_document.document_element = create_element_for_token(tmp, token);
    break;
  case ATTRIBUTE_NODE:
  case TEXT_NODE:
  case CDATA_SECTION_NODE:
  case ENTITY_REFERENCE_NODE:
  case ENTITY_NODE:
  case PROCESSING_INSTRUCTION_NODE:
  case COMMENT_NODE:
  case DOCUMENT_NODE:
    break;
  case DOCUMENT_TYPE_NODE:
    tmp_document.doctype.name = token.doctype.name;
    tmp_document.doctype.public_id = token.doctype.public_identifier;
    tmp_document.doctype.system_id = token.doctype.system_identifier;
  case DOCUMENT_FRAGMENT_NODE:
  case NOTATION_NODE:
    break;
  }
  
  tmp->owner_document = tmp_document;
  
  return tmp;
}

Node* make_text_node(Text text) {
  Node *tmp = malloc(sizeof(Node));
  tmp->node_type = TEXT_NODE;

  tmp->parent_node = NULL;
  tmp->first_child = tmp->last_child = tmp->previous_sibling = tmp->next_sibling = NULL;

  Document tmp_document;

  tmp_document.text = text;

  tmp->owner_document = tmp_document;

  return tmp;
}

Node* append_node(Node *parent, Node *node) {
  if (node->parent_node) {
    return node;
  }

  if (parent->last_child) {
    parent->last_child->next_sibling = node;
    node->previous_sibling = parent->last_child;
  }

  node->parent_node = parent;

  parent->last_child = node;

  if (!parent->first_child)
    parent->first_child = parent->last_child;

  return node;
}

// TODO: implement insert, pre_insert and append_child in spec compliant way

Node *append_child(Node *parent, Node *node) {
  return append_node(parent, node);
}
