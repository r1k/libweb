#include <LibWeb/DOM/text.h>
#include <stdlib.h>
#include <string.h>

Text text_constructor(Text text, DOMString data) {
  text.data.data = malloc(strlen(data)+1);
  strcpy(text.data.data, data);
  return text;
}

Text text_append(Text text, DOMString data) {
  text.data = append_data(text.data, data);
  return text;
}
