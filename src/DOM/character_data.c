#include <LibWeb/DOM/character_data.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

CharacterData append_data(CharacterData character_data, DOMString data) {
  // FIXME: implement in spec-compliant way
  
  size_t len = strlen(character_data.data);
  size_t data_len;

  if (sizeof(data) > 1) {
    data_len = strlen(data);
  } else { 
    data_len = 1;
  }

  character_data.data = realloc(character_data.data, len+data_len+1);
  strcpy(character_data.data+len, data);

  return character_data;
}
